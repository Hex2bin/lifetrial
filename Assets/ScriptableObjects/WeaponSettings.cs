
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponSettings", menuName = "ScriptableObjects/WeaponSettingsScriptableObject", order = 1)]
public class WeaponSettings : ScriptableObject
{
    public GameObject Projectile;
    public int MagazineSize;
    public int RealoadTime;
    public float ProjectileLifeTime;
    public float FireRate;
    public float ProjectileSpeed;
    public AudioClip ShootSound;
    public AudioClip ReloadSound;
    public AudioClip EmptyShootSound;
    public AudioClip GrabWeaponSound;
    public GameObject MuzzleEffect;
}
