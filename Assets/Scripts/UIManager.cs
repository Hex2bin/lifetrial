using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    private Animator AnimController;

    // Start is called before the first frame update
    void Start()
    {
        AnimController = GameObject.Find("Ely").GetComponent<Animator>();
        Assert.IsNotNull(AnimController, "Aly Animator not found");
        DontDestroyOnLoad(AnimController.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAnim(String AnimName)
    {
        AnimController.Play(AnimName);
    }

    public void LoadLevel(String LevelName)
    {
        SceneManager.LoadScene(LevelName);
    }
}
