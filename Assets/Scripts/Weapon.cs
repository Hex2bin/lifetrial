using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;
using static UnityEngine.ParticleSystem;

[RequireComponent(typeof(AudioSource))]
public class Weapon : MonoBehaviour
{

    public WeaponSettings WeaponSettings;

    private Transform ProjectileSpawnPoint;
    private AudioSource AudioData;
    private bool CanShoot = true;

    private List<Projectile> Projectiles;
    private List<List<ParticleSystem>> Particles;

public int RemainingAmmo { get; private set; }
    public int MaxAmmo { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        Projectiles = new List<Projectile>();
        Particles = new List<List<ParticleSystem>>();
        AudioData = GetComponent<AudioSource>();
        foreach (Transform child in this.transform)
        {
            if (child.name == "GunMuzzle")
            {
                ProjectileSpawnPoint = child;
            }
        }
        Assert.IsNotNull(ProjectileSpawnPoint, "gun muzzle not found");
        Assert.IsNotNull(WeaponSettings, "Weapon Settings not assembly");
        MaxAmmo = WeaponSettings.MagazineSize;
        RemainingAmmo = WeaponSettings.MagazineSize;
        AudioData.clip = WeaponSettings.ShootSound;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Reload()
    {
        StartCoroutine(ReloadCoroutine());
        AudioData.clip = WeaponSettings.ReloadSound;
        AudioData.Play(0);
    }

    public void Shoot()
    {
        if (CanShoot && RemainingAmmo > 0)
        {
            AudioData.clip = WeaponSettings.ShootSound;
            AudioData.Play(0);
            if (Projectiles.Count != 0)
            {
                Projectiles[Projectiles.Count - 1].transform.position = ProjectileSpawnPoint.position;
                Projectiles[Projectiles.Count - 1].transform.rotation = Camera.main.transform.rotation;
                Projectiles[Projectiles.Count - 1].ProjectileSpeed = WeaponSettings.ProjectileSpeed;
                Projectiles[Projectiles.Count - 1].gameObject.SetActive(true);
                Projectiles[Projectiles.Count - 1].PushProjectile();
                StartCoroutine(RecicleProjectile(Projectiles[Projectiles.Count - 1].gameObject));
                Projectiles.RemoveAt(Projectiles.Count - 1);
            }
            else
            {
                GameObject DummyProjectile = Instantiate(WeaponSettings.Projectile, ProjectileSpawnPoint.position, Camera.main.transform.rotation);
                DummyProjectile.GetComponent<Projectile>().ProjectileSpeed = WeaponSettings.ProjectileSpeed;
                DummyProjectile.GetComponent<Projectile>().PushProjectile();

                StartCoroutine(RecicleProjectile(DummyProjectile));
            }

            bool particleRecycle = false;
            if (Particles.Count != 0)
            {
                foreach (List<ParticleSystem> ParticlesGroup in Particles)
                {
                    bool systemStoped = false;
                    foreach (ParticleSystem Particle in ParticlesGroup)
                    {
                        systemStoped = Particle.isStopped;
                    }
                    if (systemStoped)
                    {
                        foreach (ParticleSystem Particle in ParticlesGroup)
                        {
                            Particle.transform.position = ProjectileSpawnPoint.position;
                            Particle.transform.rotation = Camera.main.transform.rotation;
                            Particle.gameObject.SetActive(true);
                        }

                        particleRecycle = true;
                        //ParticlesGroup.Clear();
                        //Particles.Remove(ParticlesGroup);
                        break;
                    }
                }
            }
            
            if(!particleRecycle)
            {
                List<ParticleSystem> DummyParticleGroup = new List<ParticleSystem>();
                foreach (ParticleSystem Particle in WeaponSettings.MuzzleEffect.GetComponentsInChildren<ParticleSystem>())
                {
                    GameObject DummyMuzzleVFX = Instantiate(Particle.gameObject, ProjectileSpawnPoint.position, Camera.main.transform.rotation, this.transform);
                    DummyParticleGroup.Add(DummyMuzzleVFX.GetComponent<ParticleSystem>());
                }
                Particles.Add(DummyParticleGroup);
            }

            CanShoot = false;
            StartCoroutine(FireDelayCoroutine());
            --RemainingAmmo;
        }
        else if (RemainingAmmo <= 0)
        {
            AudioData.clip = WeaponSettings.EmptyShootSound;
            AudioData.Play(0);
        }
    }

    IEnumerator RecicleProjectile(GameObject Projectile)
    {
        yield return new WaitForSeconds(WeaponSettings.ProjectileLifeTime);
        Projectile.SetActive(false);
        Projectiles.Add(Projectile.GetComponent<Projectile>());
    }

    IEnumerator FireDelayCoroutine()
    {
        yield return new WaitForSeconds(WeaponSettings.FireRate);
        CanShoot = true;
    }

    IEnumerator ReloadCoroutine()
    {
        yield return new WaitForSeconds(WeaponSettings.RealoadTime);
        RemainingAmmo = MaxAmmo;
        StarterAssetsInputs.updateWeaponAmmoEvent.Invoke(RemainingAmmo.ToString(), MaxAmmo.ToString());
    }
}
