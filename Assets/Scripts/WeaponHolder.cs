using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class WeaponHolder : MonoBehaviour
{
    [SerializeField] private GameObject Weapon;
    [SerializeField] private float speed = 0.15f;

    private GameObject WeaponInstance;
    private float timeCount = 0.0f;

    private float StartAngle = 0f;
    private float EndAngle = 0f;
    private float Angle = 180f;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject DummyWeapon = Instantiate(Weapon, Vector3.zero, Quaternion.Euler(-45f,0f,0f), this.transform);
        WeaponInstance = Instantiate(Weapon, this.transform);
        WeaponInstance.transform.localPosition = Vector3.zero;
        WeaponInstance.transform.localRotation = Quaternion.Euler(-45f, 0f, 0f);
        EndAngle = Angle;
    }

    // Update is called once per frame
    void Update()
    {
        //Mathf.LerpAngle(0f, 360f, timeCount * speed);
        WeaponInstance.transform.localRotation = Quaternion.Euler(-45f, Mathf.Lerp(0f, 360f, timeCount * speed), 0f);
        if (timeCount * speed >= 1) timeCount = 0;
        timeCount = timeCount + Time.deltaTime;
    }
}
