using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class GameplayUI : MonoBehaviour
{

    [SerializeField] 
    private RectTransform UIInteraction;

    [SerializeField] 
    private TMP_Text WeaponAmmoText;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(UIInteraction, "UI Interaction not assemble");
        ShowUIInteraction(false);
    }

    public void ShowUIInteraction(bool IsShowing)
    {
        UIInteraction.gameObject.SetActive(IsShowing);
    }

    public void ShowUIInteraction(bool IsShowing, Vector2 Position)
    {
        UIInteraction.gameObject.SetActive(IsShowing);
        UIInteraction.position = Position;
    }

    public void UpdateWeaponAmmoText(string AmmonRemaining, string MaxAmmo)
    {
        WeaponAmmoText.text = AmmonRemaining + " / " + MaxAmmo;
    }
}
