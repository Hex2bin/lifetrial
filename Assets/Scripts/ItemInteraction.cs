using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UIElements;

[RequireComponent(typeof(AudioSource))]
public class ItemInteraction : MonoBehaviour
{

    public float InteractionDistance = 20f;

    private FirstPersonController FPController;
    private AudioSource AudioData;
    private bool Pickable = true;
    private bool CanInteract = false;

    // Start is called before the first frame update
    void Start()
    {
        AudioData = GetComponent<AudioSource>();
        FPController = GameObject.Find("PlayerCapsule").GetComponent<FirstPersonController>();
        Assert.IsNotNull(FPController, "PlayerCapsule not found or don't have attached a FirstPersonController Component");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Distance = FPController.transform.position - transform.position;

        if (Distance.magnitude < InteractionDistance && Pickable)
        {
            if (!CanInteract)
            {
                CanInteract = true;
                FPController.InteractuableList.Add(this);
            }
        }
        else
        {
            CanInteract = false;
            if (FPController.InteractuableList.Contains(this))
            {
                FPController.InteractuableList.Remove(this);
            }
        }
    }

    public void PickupWeapon()
    {
        AudioData.clip = gameObject.GetComponent<Weapon>().WeaponSettings.GrabWeaponSound;
        AudioData.Play(0);
        Pickable = false;
        FPController.ItemInteraction.RemoveListener(PickupWeapon);
        Transform WeaponSlot = Camera.main.gameObject.transform.Find("WeaponSlot");
        foreach (Transform child in WeaponSlot)
        {
            child.parent = null;
            Destroy(child.gameObject);
        }

        Transform dummyParent = null;
        if (this.transform.parent != null)
        {
            dummyParent = this.transform.parent;
        }

        this.transform.parent = WeaponSlot;
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;

        if (dummyParent != null)
        {
            Destroy(dummyParent.gameObject);
        }
    }
}
