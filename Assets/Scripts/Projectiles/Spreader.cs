using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Assertions;

public class Spreader : Projectile
{
    [SerializeField] 
    private GameObject SpreaderBullet;
    [SerializeField] private float speed = 0.15f;
    [SerializeField]
    private int Bullets;

    private List<GameObject> SpawnedBullets;

    private float timeCount = 0.0f;

    // Start is called before the first frame update
    void Awake()
    {
        SpawnedBullets = new List<GameObject>();
        Assert.IsNotNull(SpreaderBullet, "spreader bullet not set");
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y, Mathf.Lerp(0f, 360f, timeCount * speed));
        if (timeCount * speed >= 1) timeCount = 0;
        timeCount = timeCount + Time.deltaTime;
    }

    public override void PushProjectile()
    { 
        if(SpawnedBullets == null) SpawnedBullets = new List<GameObject>();
        this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * ProjectileSpeed);

        float angle = 360f / Bullets;
        if (SpawnedBullets.Count > 0)
        {
            for (int i = 0; i < Bullets; ++i)
            {
                Vector3 VectorDirection = Quaternion.AngleAxis(angle * i, Camera.main.transform.forward) * Camera.main.transform.up;
                SpawnedBullets[i].transform.localPosition = Vector3.zero;
                SpawnedBullets[i].transform.localRotation = Quaternion.identity;
                SpawnedBullets[i].GetComponent<Rigidbody>().velocity = Vector3.zero;
                SpawnedBullets[i].GetComponent<Rigidbody>().AddForce((VectorDirection + Camera.main.transform.forward) * ProjectileSpeed);
            }
        }
        else
        {
            for (int i = 0; i < Bullets; ++i)
            {
                Vector3 VectorDirection = Quaternion.AngleAxis(angle * i, Camera.main.transform.forward) * Camera.main.transform.up;
                GameObject DummyBullet = Instantiate(SpreaderBullet, this.transform);
                DummyBullet.transform.localPosition = Vector3.zero;
                DummyBullet.transform.localRotation = Quaternion.identity;
                DummyBullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
                DummyBullet.GetComponent<Rigidbody>().AddForce((VectorDirection + Camera.main.transform.forward) * ProjectileSpeed);
                SpawnedBullets.Add(DummyBullet);
            }
        }
    }
}
