using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float ProjectileSpeed;

    public virtual void PushProjectile()
    {
        this.GetComponent<Rigidbody>().AddForce((Camera.main.transform.forward * ProjectileSpeed));
    }
}
