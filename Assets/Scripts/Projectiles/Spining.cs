using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spining : Projectile
{
    float TimeElapsed = 0.5f;
    private int ForceDirection = 1;
    private Vector3 Direction = Vector3.zero;
    private Rigidbody projectileRigidbody;
    public float OsciliationTime = 0.9f;
    private float RanAxis = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        projectileRigidbody = this.GetComponent<Rigidbody>();
    }

    public override void PushProjectile()
    {
        RanAxis = Random.value;
        Direction = (RanAxis > 0.5) ? this.transform.up : this.transform.right;
        OsciliationTime = Random.Range(0.2f, 0.8f);
        TimeElapsed = OsciliationTime/4;
        this.GetComponent<Rigidbody>().AddForce(((Camera.main.transform.forward + Direction) * ProjectileSpeed));
    }

    // Update is called once per frame
    void Update()
    {
        TimeElapsed += (Time.deltaTime * ForceDirection);
        if (TimeElapsed > OsciliationTime)
        {
            TimeElapsed = OsciliationTime;
            ForceDirection = -1;
            projectileRigidbody.velocity = Vector3.zero;
            projectileRigidbody.AddForce((this.transform.forward + (Direction * -1)) * ProjectileSpeed);
        }
        if (TimeElapsed < (OsciliationTime * -1))
        {
            TimeElapsed = (OsciliationTime * -1);
            ForceDirection = 1;
            projectileRigidbody.velocity = Vector3.zero;
            projectileRigidbody.AddForce((this.transform.forward + Direction) * ProjectileSpeed);

        }
    }
}
