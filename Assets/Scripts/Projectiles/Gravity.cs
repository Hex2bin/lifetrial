using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : Projectile
{

    [SerializeField]
    private int ActionRadius;

    private BoxCollider[] _bodies;

    

    // Start is called before the first frame update
    void Start()
    {
        _bodies = GameObject.FindObjectsOfType<BoxCollider>();
        this.GetComponent<Rigidbody>().AddForce((Camera.main.transform.forward * ProjectileSpeed));
    }

    // Update is called once per frame
    void Update()
    {
        foreach (BoxCollider bodie in _bodies)
        {
            Vector3 Distance = (transform.position - bodie.transform.position);
            Gravity dummy = bodie.gameObject.GetComponent<Gravity>();
            if (dummy == null)
            {
                bodie.GetComponent<Rigidbody>().AddForce(Distance.normalized * 10);
            }
        }
    }
}
