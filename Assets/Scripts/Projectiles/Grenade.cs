using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Projectile
{
    public override void PushProjectile()
    {
        this.GetComponent<Rigidbody>().AddForce(((Camera.main.transform.forward + new Vector3(0f, 1f, 0f)) * ProjectileSpeed));
    }
}
