using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;


#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

[System.Serializable]
public class ShootEvent : UnityEvent
{

}

[System.Serializable]
public class UpdateWeaponAmmoEvent : UnityEvent <string, string>
{

}

public class StarterAssetsInputs : MonoBehaviour
{
	[Header("Character Input Values")]
	public Vector2 move;
	public Vector2 look;
	public bool jump;
	public bool sprint;
	public bool pickup;

	[Header("Movement Settings")]
	public bool analogMovement;

	[Header("Mouse Cursor Settings")]
	public bool cursorLocked = true;
	public bool cursorInputForLook = true;

    private FirstPersonController FPController;
    public static ShootEvent shootEvent;
    public static UpdateWeaponAmmoEvent updateWeaponAmmoEvent;
    private GameplayUI GameplayUi;

    void Start()
    {
        FPController = GameObject.Find("PlayerCapsule").GetComponent<FirstPersonController>();
        shootEvent = new ShootEvent();
        updateWeaponAmmoEvent = new UpdateWeaponAmmoEvent();
        GameplayUi = GameObject.Find("UI").GetComponent<GameplayUI>();

        Assert.IsNotNull(FPController, "PlayerCapsule not found or don't have attached a FirstPersonController Component");
        Assert.IsNotNull(shootEvent, "Shoot event not initialize properly");
        Assert.IsNotNull(updateWeaponAmmoEvent, "update ammo event not initialize properly");
        Assert.IsNotNull(GameplayUi, "UI not found");

        updateWeaponAmmoEvent.AddListener(GameplayUi.UpdateWeaponAmmoText);
    }

#if ENABLE_INPUT_SYSTEM
    public void OnMove(InputValue value)
	{
		MoveInput(value.Get<Vector2>());
	}

	public void OnLook(InputValue value)
	{
		if(cursorInputForLook)
		{
			LookInput(value.Get<Vector2>());
		}
	}

	public void OnJump(InputValue value)
	{
		JumpInput(value.isPressed);
	}

	public void OnSprint(InputValue value)
	{
		SprintInput(value.isPressed);
	}

    public void OnPickup(InputValue value)
    {
        PickupInput(value.isPressed);
    }

    public void OnShoot(InputValue value)
    {
        if (value.isPressed && FPController.EquipedWeapon != null)
        {
            //FPController.EquipedWeapon.Shoot();
            shootEvent.Invoke();
            updateWeaponAmmoEvent.Invoke(FPController.EquipedWeapon.RemainingAmmo.ToString(), FPController.EquipedWeapon.MaxAmmo.ToString());
        }
    }
    public void OnReload(InputValue value)
    {
        if (value.isPressed && FPController.EquipedWeapon != null)
        {
            FPController.EquipedWeapon.Reload();
        }
    }
#endif


    public void MoveInput(Vector2 newMoveDirection)
	{
		move = newMoveDirection;
	} 

	public void LookInput(Vector2 newLookDirection)
	{
		look = newLookDirection;
	}

	public void JumpInput(bool newJumpState)
	{
		jump = newJumpState;
	}

    public void PickupInput(bool NewPickupState)
    {
        pickup = NewPickupState;
        if (pickup)
        {
            FPController.PerformItemInteraction();
        }
        print("Pickup");
    }

    public void SprintInput(bool newSprintState)
	{
		sprint = newSprintState;
	}
	
	private void OnApplicationFocus(bool hasFocus)
	{
		SetCursorState(cursorLocked);
	}

	private void SetCursorState(bool newState)
	{
		Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
	}
}